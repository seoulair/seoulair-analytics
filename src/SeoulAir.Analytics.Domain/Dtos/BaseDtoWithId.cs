namespace SeoulAir.Analytics.Domain.Dtos
{
    public class BaseDtoWithId
    {
        public string Id { get; set; }
    }
}
