using SeoulAir.Analytics.Domain.Dtos;

namespace SeoulAir.Analytics.Domain.Interfaces.Repositories
{
    public interface ICriticalAlertRepository : ICrudBaseRepository<CriticalAlertDto>
    {
        
    }
}
