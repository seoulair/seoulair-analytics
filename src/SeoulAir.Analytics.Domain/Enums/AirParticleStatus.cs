namespace SeoulAir.Analytics.Domain.Enums
{
    public enum AirParticleStatus : byte
    {
        Good = 1,
        Normal,
        Bad,
        VeryBad
    }
}
