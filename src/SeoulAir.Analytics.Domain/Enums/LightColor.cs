namespace SeoulAir.Analytics.Domain.Enums
{
    public enum LightColor : byte
    {
        Blue = 1,
        Green,
        Yellow,
        Red
    }
}
