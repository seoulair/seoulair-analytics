namespace SeoulAir.Analytics.Domain.Resources
{
    public static class Strings
    {
        #region Errors and warnings messages
        
        public const string ParameterNullOrEmptyMessage = "Parameter {0} must not be null or empty string.";
        public const string ParameterBetweenMessage = "Value of parameter {0} must be between {1} and {2}.";
        public const string PaginationOrderError = "Pagination error. Invalid \"Order By\" option: {0}";
        public const string PaginationFilterError = "Pagination error. Invalid \"Filter by\" option: {0}";
        public const string InvalidParameterValueMessage = "Value of parameter {0} has invalid value.";
        public const string RequestBodyGetException = "Http method GET does not support request body.";

        #endregion
        
        #region Swagger Documentation

        public const string OpenApiInfoProjectName = "SeoulAir.Analytics API";
        public const string OpenApiInfoTitle = "SeoulAir Analytics microservice.";
        public const string OpenApiInfoProjectVersion = "1.0.0";
        public const string OpenApiInfoDescription
            = "SeoulAir Analytics is microservice that is part of SeoulAir project.\n" +
              "For more information visit Gitlab Repository";
        public const string SwaggerEndpoint = "/swagger/{0}/swagger.json";
        public const string GitlabContactName = "Gitlab Repository";
        public const string GitlabRepoUri = "http://gitlab.com/seoulair/seoulair-analytics.git";

        #endregion
    }
}
