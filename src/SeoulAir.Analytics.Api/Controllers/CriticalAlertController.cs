using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeoulAir.Analytics.Domain.Dtos;
using SeoulAir.Analytics.Domain.Interfaces.Services;

namespace SeoulAir.Analytics.Api.Controllers
{
    /// <summary>
    /// Responsible for all CRUD operations on manual creation of CriticalAlerts.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CriticalAlertController : ControllerBase
    {
        private readonly ICriticalAlertService _criticalAlertService;

        public CriticalAlertController(ICriticalAlertService criticalAlertService)
        {
            _criticalAlertService = criticalAlertService;
        }
        
        /// <summary>
        /// Read (CRUD) operation. Gets the CriticalAlert from MongoDb Database. Matches the record by Id.  
        /// </summary>
        /// <param name="id">Unique string that represents the entity in database.</param>
        /// <response code="200">Operation completed successfully, requested resource found and returned</response>
        /// <response code="204">Operation completed successfully, requested resource does not exist</response>
        [ProducesResponseType(typeof(DataRecordDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpGet]
        public async Task<ActionResult<CriticalAlertDto>> GetByIdAsync(string id)
        {
            var result = await _criticalAlertService.GetByIdAsync(id);

            if (result == default)
                return NoContent();

            return Ok(result);
        }
        
        /// <summary>
        /// Reads multiple entries from database but in paginated form.
        /// </summary>
        /// <param name="paginator">Parameters for generating page.</param>
        /// <response code="200">Operation completed successfully, one page returned</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("paginated")]
        public async Task<ActionResult<PaginatedResultDto<CriticalAlertDto>>> GetPaginatedAsync(
            [FromQuery] Paginator paginator)
        {
            return Ok(await _criticalAlertService.GetPaginatedAsync(paginator));
        }
        
        /// <summary>
        /// Create (CRUD) operation. Creates the CriticalAlert instance in MongoDb Database.
        /// </summary>
        /// <param name="criticalAlert">CriticalAlert to be created.</param>
        /// <remarks>
        /// Id should not be passed. If it is passed it is going to be ignored.
        /// Id is generated by the system
        /// </remarks>
        /// <response code="201">Operation completed successfully,
        /// data record has been created with returned id.</response>
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<string>> Post(CriticalAlertDto criticalAlert)
        {
            string createdId = await _criticalAlertService.AddAsync(criticalAlert);

            return CreatedAtAction(nameof(Post), createdId);
        }
        
        /// <summary>
        /// Deletes (CRUD) operation. Deletes CriticalAlert from the database.
        /// </summary>
        /// <param name="id">Unique string that represents the entity to be deleted.</param>
        /// <response code="204">Operation completed successfully, CriticalAlert has been deleted from database</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(string id)
        {
            await _criticalAlertService.DeleteAsync(id);

            return NoContent();
        }
        
        /// <summary>
        /// Updates (CRUD) operation. Updates CriticalAlert from the database.
        /// </summary>
        /// <param name="criticalAlert">New state of alert that needs to be updated.</param>
        /// <response code="204">Operation completed successfully, CriticalAlert has been updated</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut]
        public async Task<ActionResult> UpdateAsync(CriticalAlertDto criticalAlert)
        {
            await _criticalAlertService.UpdateAsync(criticalAlert);

            return NoContent();
        }
    }
}